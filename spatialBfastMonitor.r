# Install necessary packages, as needed
requiredPackages <- c("devtools","rgdal","stars","pbapply")
newPackages <- requiredPackages[!(requiredPackages %in% installed.packages()[,"Package"])]
if(length(newPackages)) install.packages(newPackages)
devtools::install_github(c("bfast2/strucchange","bfast2/bfast"), quiet = TRUE)

# Read inputs
arguments <- commandArgs(trailingOnly = TRUE)
inputFilepath <- arguments[1] #"C:\\bfast-test\\NDMI_2010-01-01_2020-06-01.tif"
outputFilepath <- arguments[2] #"C:\\bfast-test\\NDMI_BFAST.tif"

# Variables that could be arguments.
# Check ?bfast::bfastmonitor for details
# Probably this is the only section that you should edit
monitoringStartDate <- c(2014,1) # make sure this is inside the range of observationDates
regressionFormula <- response ~ harmon
harmonicOrder <- 1
historyStartDate <- "all" # I know it's weird, check ?bfast::bfastmonitor to understand
pbapply::pboptions(type="timer")

# Validate inputs
indexedRasterTimeSeries <- stars::read_stars(inputFilepath)
observationDates <- read.csv(paste0(tools::file_path_sans_ext(inputFilepath),".csv"),header = T)[,2]
onWrongDates <- function(cond){
  print(cond)
  stop("
  There is something wrong with the dates
  The dates csv must have a header line,
  The dates must be on the second column,
  The dates must be in the YYYY-MM-dd format,
  -------------------------------------------
  E.g. csv content:

  system:index,Date,.geo
  0,2010-02-13,
  1,2010-03-09,
  2,2010-04-10,
  3,2010-04-18,
       ")
}

suppressMessages(invisible(tryCatch(bfast::bfastts(rep(1,length(observationDates)),observationDates,"irregular"),
         error = onWrongDates)))

# Define the pixel-wise function
bfastMonitorLayers <- function(pixelValues, dates, startDate,...)
{
  pixelTimeSeries <-  bfast::bfastts(pixelValues, dates = dates, type="irregular")
  is.null(startDate)
  result <- tryCatch({
    breakMagnitude <- bfast::bfastmonitor(pixelTimeSeries, startDate, ...)
    c(breakpoint = breakMagnitude$breakpoint,
      magnitude = breakMagnitude$magnitude,
      error = FALSE)
  },
  error=function(cond){
    c(breakpoint = NA,
      magnitude = NA,
      error = TRUE)
  })
}

# Parallelization stuff, probably not necessary to touch
invisible({
  cluster <-  parallel::makeCluster(parallel::detectCores()-1)
  parallel::clusterEvalQ(cluster, library(strucchange))
  parallel::clusterEvalQ(cluster, bfast::set_fast_options()) # at the time of this script, it makes runs 38% shorter
})

# The actual call to process
output <- stars::st_apply(X = indexedRasterTimeSeries, MARGIN = c("x","y"), FUN = bfastMonitorLayers, dates = observationDates, startDate = monitoringStartDate, formula = regressionFormula, order = harmonicOrder, history = historyStartDate, CLUSTER = cluster, PROGRESS = TRUE)

# More parallelization
parallel::stopCluster(cluster)

# Write output to file
stars::write_stars(output,outputFilepath)